# recursive-components

This prototype showcases a recursive component, done by following the LogRocket
tutorial [Vue recursive components: Rendering nested comments][1]

[1]: https://blog.logrocket.com/rendering-nested-comments-recursive-components-vue/

This example uses Vue 3 with TypeScript.
